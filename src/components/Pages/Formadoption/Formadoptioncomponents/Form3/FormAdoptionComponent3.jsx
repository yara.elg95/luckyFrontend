import React from "react";
import {useForm} from "react-hook-form";
import "./FormAdoptionComponent3.scss"
import DialogFormAdoption from "../DialogFormAdoption/DialogFormAdoption";
import {ProgressBar} from "primereact/progressbar";

function FormAdoptionComponent3(props) {

    const {register, handleSubmit} = useForm(0);

    const onSubmit = data => {
        props.addAnimalData(data);
    }

    return (
        <form className="c-form-adoption" onSubmit={handleSubmit(onSubmit)}>
            <p className="c-form-adoption__title">
                Formulario de adopción
            </p>
            <ProgressBar className="c-form-adoption__progress" showValue={false} value={70}/>
            <p className="c-form-adoption__sub-title">
                Familia y hogar
            </p>
            <div className="c-form-adoption__container">
                <label className="c-form-adoption__question">¿Dónde vives?</label>
                <input className="c-form-adoption__input" placeholder="Piso, casa, chalet..." name="place"
                       ref={register} type="text" required={true}/>
            </div>
            <div className="c-form-adoption__container">
                <div className="c-form-adoption__question-container">
                    <label className="c-form-adoption__question">¿Vives de alquiler?</label>
                    <div className="c-form-adoption__yes-no">
                        <div className="c-form-adoption__radio-container">
                            <label className="c-form-adoption__res">Sí</label>
                            <input className="c-form-adoption__input-radio" name="rent" value={false} ref={register}
                                   type="radio" required={true}/>

                        </div>
                        <div className="c-form-adoption__radio-container">
                            <label className="c-form-adoption__res">No</label>
                            <input className="c-form-adoption__input-radio" name="rent" value={false} ref={register}
                                   type="radio" required={true}/>

                        </div>
                    </div>
                </div>
                <div className="c-form-adoption__question-container">
                    <label className="c-form-adoption__question">¿Tu casero permite animales?</label>
                    <div className="c-form-adoption__yes-no">
                        <div className="c-form-adoption__radio-container">
                            <label className="c-form-adoption__res">Sí</label>
                            <input className="c-form-adoption__input-radio" name="lessor" value={false}
                                   ref={register}
                                   type="radio" required={true}/>

                        </div>
                        <div className="c-form-adoption__radio-container">
                            <label className="c-form-adoption__res">No</label>
                            <input className="c-form-adoption__input-radio" name="lessor" value={false}
                                   ref={register}
                                   type="radio" required={true}/>

                        </div>
                    </div>
                </div>
                <div className="c-form-adoption__question-container">
                    <label className="c-form-adoption__question">¿Crees que podrías mudarte pronto?</label>
                    <div className="c-form-adoption__yes-no">
                        <div className="c-form-adoption__radio-container">
                            <label className="c-form-adoption__res">Sí</label>
                            <input className="c-form-adoption__input-radio" name="movingHouse" value={false}
                                   ref={register}
                                   type="radio" required={true}/>

                        </div>
                        <div className="c-form-adoption__radio-container">
                            <label className="c-form-adoption__res">No</label>
                            <input className="c-form-adoption__input-radio" name="movingHouse" value={false}
                                   ref={register}
                                   type="radio" required={true}/>

                        </div>
                    </div>
                </div>
                <div className="c-form-adoption__question-container">
                    <label className="c-form-adoption__question">¿Tiene jardín?</label>
                    <div className="c-form-adoption__yes-no">
                        <div className="c-form-adoption__radio-container">
                            <label className="c-form-adoption__res">Sí</label>
                            <input className="c-form-adoption__input-radio" name="garden" value={false} ref={register}
                                   type="radio" required={true}/>

                        </div>
                        <div className="c-form-adoption__radio-container">
                            <label className="c-form-adoption__res">No</label>
                            <input className="c-form-adoption__input-radio" name="garden" value={false} ref={register}
                                   type="radio" required={true}/>

                        </div>
                    </div>
                </div>
                <div className="c-form-adoption__question-container">
                    <label className="c-form-adoption__question">¿Vives con otras personas?</label>
                    <div className="c-form-adoption__yes-no">
                        <div className="c-form-adoption__radio-container">
                            <label className="c-form-adoption__res">Sí</label>
                            <input className="c-form-adoption__input-radio" name="coexist" value={false} ref={register}
                                   type="radio" required={true}/>

                        </div>
                        <div className="c-form-adoption__radio-container">
                            <label className="c-form-adoption__res">No</label>
                            <input className="c-form-adoption__input-radio" name="coexist" value={false}
                                   ref={register}
                                   type="radio" required={true}/>

                        </div>
                    </div>
                </div>
                <div className="c-form-adoption__question-container">
                    <label className="c-form-adoption__question">¿Están todos de acuerdo con la adopción?</label>
                    <div className="c-form-adoption__yes-no">
                        <div className="c-form-adoption__radio-container">
                            <label className="c-form-adoption__res">Sí</label>
                            <input className="c-form-adoption__input-radio" name="inagreement" value={false}
                                   ref={register}
                                   type="radio" required={true}/>

                        </div>
                        <div className="c-form-adoption__radio-container">
                            <label className="c-form-adoption__res">No</label>
                            <input className="c-form-adoption__input-radio" name="inagreement" value={false}
                                   ref={register}
                                   type="radio" required={true}/>

                        </div>
                    </div>
                </div>
                <div className="c-form-adoption__question-container">
                    <label className="c-form-adoption__question">¿Estás de acuerdo con que visitemos tu casa?</label>
                    <div className="c-form-adoption__yes-no">
                        <div className="c-form-adoption__radio-container">
                            <label className="c-form-adoption__res">Sí</label>
                            <input className="c-form-adoption__input-radio" name="visitHouse" value={false}
                                   ref={register}
                                   type="radio" required={true}/>

                        </div>
                        <div className="c-form-adoption__radio-container">
                            <label className="c-form-adoption__res">No</label>
                            <input className="c-form-adoption__input-radio" name="visitHouse" value={false}
                                   ref={register}
                                   type="radio" required={true}/>

                        </div>
                    </div>
                </div>
            </div>

            <DialogFormAdoption/>
        </form>
    );
}

export default FormAdoptionComponent3;
