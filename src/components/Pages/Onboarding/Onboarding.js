import React, {useState} from "react";
import {Carousel} from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./Onboarding.scss"
import { useHistory } from 'react-router-dom';


export function Onboarding() {

    const [valor, setValor] = useState(0);
    const history = useHistory()
    const images = [
        {
            path: process.env.PUBLIC_URL + "/assets/images/undrawGoodDoggy4Wfq.png",
            title: "Encuentra todo tipo de servicios que tienes cerca de ti"
        },
        {
            path: process.env.PUBLIC_URL + "/assets/images/imagen2.png",
            title: "Adopta desde tu móvil",
            text: "Puedes acceder al perfil de muchos animales que están en adopción y filtrarlos para encontrar el que mejor se adapte a ti"
        },
        {
            path: process.env.PUBLIC_URL + "/assets/images/undrawPetAdoption2Qkw.png",
            title: "Si eres una asociación sube a tus peludos para darles mas difusión"
        }]

    return (
        <>
            <Carousel className="c-carousel" onChange={index => {setValor(index)}}>
                {images.map((item, index) =>
                    <div className="c-carousel__container" key={index}>
                            <img className="c-carousel__img" src={item.path} alt="img"/>
                            <p className="c-carousel__title">{item.title}</p>
                            <p className="c-carousel__text">{item.text}</p>
                        </div>
                )}
            </Carousel>
            <div>
                {valor==2 ?
                    <button className="boton2" onClick={()=> history.push('home')}> Entrar</button>
                        : <></>}
            </div>
        </>

    );
}




