import React, { useState } from 'react';
import './Filtro.scss';
import { Dialog } from 'primereact/dialog';

export function FiltroApp() {
    const [visible, setVisible,] = useState();

    return (

        <>
            <Dialog  header="" visible={visible} style={{ width: '80vw' }} modal={true} onHide={() => setVisible(false)}>
          
                <div>
                    <div>
                        <h4 className="titulo">Filtros</h4>
                    </div>
                    <div className="row">
                        <div className="col-4">
                            <div className="Ejemplo">
                                <img src="assets/veterinario.png" alt="" />
                                <caption>Veterinario</caption>
                            </div>
                        </div>

                        <div className="col-4">
                            <div className="Ejemplo">
                                <img src="assets/peluqueria.png" alt="" />
                                <caption>Peluqueria</caption>
                            </div>
                        </div>

                        <div className="col-4">
                            <div className="Ejemplo">
                                <img src="assets/cafe.png" alt="" />
                                <caption>Pet friendly</caption>
                            </div>
                        </div>


                        <div className="col-4">
                            <div className="Ejemplo">
                                <img src="assets/educacion.png" alt="" />
                                <caption>Educacion</caption>
                            </div>
                        </div>

                        <div className="col-4">
                            <div className="Ejemplo">
                                <img src="assets/guarderia.png" alt="" />
                                <caption>Guarderia</caption>
                            </div>
                        </div>

                        <div className="col-4">
                            <div className="Ejemplo">
                                <img src="assets/tienda.png" alt="" />
                                <caption>Tienda</caption>
                            </div>
                        </div>
                        <div className="button-container">
                        <button className="b-filter1">Borrar filtros</button>
                        <button className="b-filter">Aplicar</button>
                    </div>
                    </div>
                </div>
            </Dialog>
            
            <button label="Show" icon="pi pi-info-circle"  onClick={(e) => setVisible(true)} />

        </>
    );
};
