import React from "react";
import AdoptCarousel from "./CompomentsAdop/AdoptCarousel";
import AdoptNovedades from "./CompomentsAdop/AdoptNovedades";
import BoxMenu from "../../Shared/BoxMenu";

function Adopcion() {

    return (
        <div>
            <AdoptCarousel/>
            <AdoptNovedades/>
            <BoxMenu/>
        </div>

    );
}

export default Adopcion;
