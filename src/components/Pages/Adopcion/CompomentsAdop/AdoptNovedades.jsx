import React, {useEffect, useState} from "react";
import './AdoptNovedades.scss'
import axios from "axios";


function AdoptNovedades() {

    const [AdoptNovedades, setAdoptNovedades] = useState([]);

    useEffect(() => {
        // PASO 2
        axios.get('http://35.180.135.174:3000/adopts')
            .then(function (response) {
                setAdoptNovedades(response.data);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            })
    }, []);


    return (
        <section className="b-novedades">

            <p className="b-novedades__seccion-titulo">Animales en Adopción</p>
            {AdoptNovedades.map((item, index) =>
                <div key={index} className="b-novedades__fondo ">
                    <div className="relativo">
                        <img className="b-novedades__img" src={item.img} alt={item.title}/>
                        <div className="corazon">
                        <img onClick={() => {alert("Me alegro que te guste!!!")}}  src="assets/iconos/favoritos.png"/>
                        {/*<img src="assets/iconos/favoritosRelleno.png"/>*/}
                    </div>
                    </div>
                    <div className="column ubicacion cajadeinfo">
                        <div className="row  ">
                            <div className="b-adopcion__ubicacion__titulo padingcajitanombre">{item.title}</div>
                            <div className="column padingcajitaanimal">
                                <div className="b-adopcion__ubicacion__ciudad padingcajitaanimal">{item.city}</div>
                                <div className="b-adopcion__ubicacion__texto padingcajitaanimal">{item.text}</div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </section>
    );
}

export default AdoptNovedades;