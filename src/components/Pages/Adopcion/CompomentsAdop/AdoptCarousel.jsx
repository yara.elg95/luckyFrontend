import React from "react";
import {Carousel} from "react-responsive-carousel";
import "./AdoptCarousel.scss"
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader


function AdoptCarousel() {

    const homeCarouselOptions = [
        {
            icon: process.env.PUBLIC_URL + 'assets/iconos/perrop@3x.png',
            title: "Apolo",
            
        },
        {
            icon: process.env.PUBLIC_URL + 'assets/iconos/ave@3x.png',
            title: "Kiko",
            
        },
        {
            icon: process.env.PUBLIC_URL + 'assets/iconos/ave@3x.png',
            title: "Dalí",
            
        }
    ];

    return (
        <Carousel className="c-Adoptcarousel-adopcion">
            {homeCarouselOptions.map((item, index) =>
                <div className="c-carousel-adopcion__container" key={index}>
                    <img className="c-carousel-adopcion__img" src={item.icon} alt="Adopcion"/>
                    <div className="c-carousel-adopcion__content-box">
                        <p className="c-carousel-adopcion__titulo">{item.title}</p>
                    </div>
                </div>
            )}
        </Carousel>
    );
}

export default AdoptCarousel;
