import React, { useContext, useState } from 'react';
import {LoginPage, sendDataLogin} from "./loginController";
import {useForm} from 'react-hook-form'
import './login.scss'

    export function Login () {
        const [shown, setShown] = React.useState(false);
        const {handleSubmit,register,errors} = useForm();
        const [pass, setPass] = useState("password");
        const submitFunction = data => {
            sendDataLogin(data);
        }

        const switchShown = () => setShown(!shown);
        const [password, setPassword] = React.useState('');const onChange = ({ currentTarget }) => setPassword(currentTarget.value);

            return (
                <div className="c-login">
                    <div className="c-login__box">
                        <img className="c-login__img" src={process.env.PUBLIC_URL + '/assets/Logo.png'}
                             alt="logo-lucky"/>
                        <p className="c-login__text">¡Hola! para continuar, inicia sesión
                            o crea una cuenta</p>
                    </div>
                    <form className="c-login__form" onSubmit={handleSubmit(submitFunction)}>
                        <label for="email">
                            <input className="c-login__input" type="email" name="email" placeholder="Email"
                                   ref={register}/>
                        </label><br/>
                        <div>
                            <input className="c-login__input c-login__input--password" name="password"
                                   placeholder="Contraseña"
                                   onChange={onChange}
                                   type={shown ? 'text' : 'password'}
                                   value={password}
                                   ref={register({required: true})}/>


                            <img className="c-login__eye"
                                 onClick={switchShown}/>
                        </div>
                        <p className="c-login__forgotten">¿Has olvidado tu contraseña?</p>

                        <div className="c-login__button-box">
                            <button className="c-login__button" type="submit">
                                Iniciar sesión
                            </button>
                            <button className="c-login__button c-login__button--white" type="button"
                                    onClick={() => console.log('clicked')}>
                                Crear cuenta
                            </button>
                        </div>
                    </form>
                </div>
            );
        }

