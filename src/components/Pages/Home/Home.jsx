import React from "react";
import HomeCarousel from "./CompomentsHome/HomeCarousel";
import Novedades from "./CompomentsHome/Novedades";
import './Home.scss';

import BoxMenu from "../../Shared/BoxMenu";

function Home() {

    return(
        <div>
            <div className="contenido">
                <HomeCarousel/>
                <Novedades/>
            </div>
            <BoxMenu/>
        </div>
    );
}

export default Home;
