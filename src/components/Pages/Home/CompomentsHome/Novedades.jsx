import './Novedades.scss';
import React, {useEffect, useState} from "react";
import axios from "axios";


function Novedades() {
    const [BlogNovedades, setBlogNovedades] = useState([]);
    
    useEffect(() => {
        // PASO 2.
        axios.get('http://35.180.135.174:3000/post')
            .then(function (response) {
                // handle success
                // SETEAR LA VARIABLE PARA TRABAJAR CON ELLA
                setBlogNovedades(response.data);
              console.log(response.data);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            })
    }, []);

    console.log (BlogNovedades[0]);

        return(
            <section className="b-novedades">
                <div className="b-novedades__raya" />
            <p className="b-novedades__seccion-titulo">Novedades</p>
            {BlogNovedades.map((item, index) =>
              <div key={index} className="b-novedades__fondo">
                  <img className="b-novedades__img" src={item.img} alt={item.title}/>
                  <p className="b-novedades__titulo">{item.title}</p>
              </div>
              
            )}
        </section>
    );
}

export default Novedades;