import React, {useState,useEffect} from 'react';
import './Splash.scss'
import Redirect from "react-router-dom/es/Redirect";


export function Splash (props) {
    const [state, setState] = useState( {redirect : false})    ;

    const componentDidMount = () => {
        setTimeout(() => setState({redirect: true}), 3000);
    }

    useEffect(() => {
        componentDidMount();
    }, []);



    return (
        state.redirect ?
            <Redirect to="/onboarding"/> :
            <div className="b-Bootstrap-splash"><img src="/assets/images/Lucky.png" alt=""/></div>)
}
