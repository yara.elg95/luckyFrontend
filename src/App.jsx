import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {Onboarding} from "./components/Pages/Onboarding/Onboarding";
import Home from "./components/Pages/Home/Home";
import Adopcion from "./components/Pages/Adopcion/Adopcion";
import FormAdoption from "./components/Pages/Formadoption/Formadoption";
import {Splash} from "./components/Pages/Splash/CompomentSplash/Splash";
import BoxMenu from "./components/Shared/BoxMenu"
import {Login} from "./components/Pages/Login/login";

function App() {


    return (

      <div className="App">
          <Router>
              <Switch>
                  <Route path="/onboarding">
                      <Onboarding/>
                  </Route>
                  <Route path="/home">
                     <Home/>
                  </Route>
                  <Route path="/adopcion">
                      <Adopcion/>
                  </Route>
                  <Route path="/filtros">
                      {/*<Chronologic/>*/}
                  </Route>
                  <Route path="/formadopcion">
                      <FormAdoption/>
                  </Route>
                  <Route path="/caja">
                      <BoxMenu/>
                  </Route>
                  <Route path="/login">
                      <Login/>
                  </Route>
                  <Route path="/">
                      <Splash/>
                  </Route>
              </Switch>


            </Router>
        </div>
    );
}

export default App;
